from django.db import models
from django.dispatch import receiver
# from django.contrib.auth.models import User
from django.conf import settings

import os


DESTINATION_CHOICES = (
        ('1', 'Email'),
        ('2', 'Telephone'),
        ('3', 'FTP'),
        ('4', 'HTTP')
    )

User = settings.AUTH_USER_MODEL


class Parameter(models.Model):
    name = models.TextField()
    value = models.TextField()

    def __repr__(self):
        return f"{self.name}: {self.value}"

    def __str__(self):
        return self.name

    def as_dict(self):
        return {self.name: self.value}


class Button(models.Model):

    name = models.CharField(max_length=50)
    destination = models.CharField(max_length=1, choices=DESTINATION_CHOICES)
    email = models.EmailField(blank=True)
    tel = models.CharField(max_length=30, blank=str)
    username = models.CharField(max_length=30, blank=True)
    password = models.CharField(max_length=30, blank=True)
    hostname = models.CharField(max_length=30, blank=True)
    application_security_key = models.CharField(max_length=30, blank=True)
    http_address = models.URLField(blank=True)
    port = models.CharField(max_length=5, blank=True)
    require_amount = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name}"

    def as_dict(self):
        if self.destination == "1":
            d = {
                "id": self.pk,
                "name": self.name,
                "destination": "email",
                "email": self.email
            }
        elif self.destination == "2":
            d = {
                "id": self.pk,
                "name": self.name,
                "destination": "tel",
                "tel": self.tel
            }
        elif self.destination == "4":
            d = {
                "id": self.pk,
                "name": self.name,
                "destination": "http",
                "host": self.http_address,
                'user': self.username,
                'pass': self.password,
                'app-sec-key': self.application_security_key,
                "ask_amount": self.require_amount
            }
        else:
            d = {
                "id": self.pk,
                "name": self.name,
                'destination': 'ftp',
                'user': self.username,
                'pass': self.password,
                'host': self.hostname,
                'port': self.port
            }
        return d


class Config(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    company = models.CharField(max_length=30)
    office = models.CharField(max_length=30)
    terminal = models.CharField(max_length=30)
    # udid = models.CharField(max_length=15, blank=True)
    button = models.ManyToManyField(Button, blank=True)
    # imageFilterId = models.CharField(max_length=1, default=1)
    max_size = models.CharField(max_length=4, default=800)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.company} {self.office} {self.terminal}"

    def as_dict(self):
        terminal = {
                "company": self.company,
                "office": self.office,
                "terminal": self.terminal,
                "max_size": self.max_size
            }
        d = []
        for but in self.button.all():
            print(but.as_dict())
            d.append({**terminal, **but.as_dict()})
        return d


class History(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="history")
    destination = models.CharField(max_length=1, choices=DESTINATION_CHOICES)
    device_type = models.CharField(max_length=10, blank=True)
    time = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.user}"


class LanguageFile(models.Model):
    name = models.CharField(max_length=30)
    short_code = models.CharField(max_length=3)
    file = models.FileField(upload_to="languages")
    # url =

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


@receiver(models.signals.post_delete, sender=History)
@receiver(models.signals.post_delete, sender=LanguageFile)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    try:
        if instance.image:
            if os.path.isfile(instance.image.path):
                os.remove(instance.image.path)
    except:
        if instance.file:
            if os.path.isfile(instance.file.path):
                os.remove(instance.file.path)


