from rest_framework import serializers
from .models import History


class HistorySerializer(serializers.ModelSerializer):
    user = serializers.CharField(
        source='config.user',
        read_only=True
    )
    image = serializers.ImageField(
        max_length=None,
        use_url=True
    )

    class Meta:
        model = History
        # fields = ('udid', 'image', 'time', 'status')
        fields = ('user', 'device_type', 'destination', 'image', 'status')

    def create(self, validated_data):
        return History.objects.create(**validated_data)
