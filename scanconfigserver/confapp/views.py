import os.path

from django.http import HttpResponse, JsonResponse
from confapp.serializer import HistorySerializer
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.http import FileResponse, HttpResponseForbidden
from rest_framework import status
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from django.conf import settings
from rest_framework.permissions import IsAuthenticated
from accounts.authentication import ExpiringTokenAuthentication
from .models import Config, Parameter, LanguageFile
import json

UserModel = get_user_model()

_UDID_NOT_EXIST = "Please contact A Bet A support on 01420 549988 to set up this new scanner, please quote reference %s"
_USER_NOT_PROVIDED = "not allowed"
_UDID_NOT_PROVIDED = "UDID not provided"


@api_view(['POST'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def config(request):
    udid = request.POST.get('udid')
    if udid is None:
        response = {
            'data': [],
            'message': _UDID_NOT_PROVIDED
        }
        return HttpResponse(json.dumps(response), content_type="application/json")

    try:
        response = {'data': []}
        User = UserModel.objects.get(username=request.user)
        c = Config.objects.get(user=User)
        signature_text = Parameter.objects.filter(name="signature_text")

        if c.active:
            response = {
                'data': c.as_dict(),
                'signature_text': signature_text[0].as_dict()['signature_text']
            }
        if len(response['data']) > 0:
            return HttpResponse(json.dumps(response), content_type="application/json")
        else:
            response = {
                'data': [],
                'message': _UDID_NOT_EXIST % udid
            }
            return HttpResponse(json.dumps(response), content_type="application/json")
    except Config.DoesNotExist:
        response = {
            'data': [],
            'message': _UDID_NOT_EXIST % udid
        }
        return HttpResponse(json.dumps(response), content_type="application/json")


class HistoryView(APIView):
    parser_classes = [FormParser, MultiPartParser]
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):
        """
        curl -X POST -S -H 'Autorization:token 34c9cd58285e76906c4ffacc6c6b5ec96b056e79' -H 'Accept:application/json' -H 'Content-Type:multipart/form-data' -F "user=administrator" -F "image=C:\\Users\\davo6\\Desktop\\bet_logo.jpg" http://35.214.71.114/upload-scan/
        """
        user = request.user
        serializer = HistorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(
                user=UserModel._default_manager.get(username=user),
                image=request.data.get('image')
            )
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def params(request):
    response = {}
    p = Parameter.objects.all()
    if len(p) == 0:
        response = {"signature_text": "Sent from scanner app"}
    else:
        for item in p.values():
            response[item["name"]] = item["value"]
    return HttpResponse(json.dumps(response))


@api_view(['POST'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def language(request):
    if request.POST.get("id"):
        L = LanguageFile.objects.get(id=request.POST["id"])
    else:
        L = list(LanguageFile.objects.all().values("id", "name", "short_code", "file"))
    return JsonResponse(L, safe=False)
