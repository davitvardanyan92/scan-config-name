from django.contrib import admin
from django import forms
from django.utils.html import mark_safe

from confapp.models import Config, History, Button, Parameter, LanguageFile

# Register your models here.


class ButtonForm(forms.ModelForm):
    DESTINATION_CHOICES = (
        ('1', 'Email'),
        ('2', 'Telephone'),
        ('3', 'FTP'),
        ('4', 'HTTP')
    )

    destination = forms.ChoiceField(choices=DESTINATION_CHOICES)


class ButtonAdmin(admin.ModelAdmin):
    class Media:
        js = ('//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',  # jquery
              '/static/admin/js/hide_attribute.js',)

    form = ButtonForm
    list_display = ("name", "get_destination_display")

class ConfigAdmin(admin.ModelAdmin):


    # list_display = ("user", "company", "office", "terminal", "get_destination_display")
    list_display = ("user", "company", "office", "terminal")
    # list_filter = ("user", "company",)
    filter_horizontal = ('button',)


class HistoryAdmin(admin.ModelAdmin):
    readonly_fields = ["user", "destination", "image", "device_type", "time", "status", "document"]
    # readonly_fields = ["user", "destination", "image", "device_type", "time", "status", "image_image"]
    # list_display = ("user", "get_destination_display", "device_type", "time", "status", "image_image")


    def document(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.image.url,
            width=obj.image.width,
            height=obj.image.height,
        )
        )


    fields = ["user", "destination", "image", "device_type", "time", "status", "document"]


class ParameterAdmin(admin.ModelAdmin):
    list_display = ("name", "value")


class LanguageFileAdmin(admin.ModelAdmin):
    list_display = ("name", "short_code", "file")


admin.site.register(Config, ConfigAdmin)
admin.site.register(History, HistoryAdmin)
admin.site.register(Button, ButtonAdmin)
admin.site.register(Parameter, ParameterAdmin)
admin.site.register(LanguageFile, LanguageFileAdmin)
