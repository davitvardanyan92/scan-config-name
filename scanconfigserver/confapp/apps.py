from django.apps import AppConfig


class ConfappConfig(AppConfig):
    name = 'confapp'
