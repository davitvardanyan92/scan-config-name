function showEmail() {
    $('.field-tel').hide();
    $('.field-email').show();
    $('.field-username').hide();
    $('.field-password').hide();
    $('.field-hostname').hide();
    $('.field-port').hide();
    $('.field-http_address').hide();
    $('.field-application_security_key').hide();
    $('.field-require_amount').hide();


}
function showTel(){
    $('.field-tel').show();
    $('.field-email').hide();
    $('.field-username').hide();
    $('.field-password').hide();
    $('.field-hostname').hide();
    $('.field-port').hide();
    $('.field-http_address').hide();
    $('.field-application_security_key').hide();
    $('.field-require_amount').hide();
}

function showFtp() {
    $('.field-tel').hide();
    $('.field-email').hide();
    $('.field-username').show();
    $('.field-password').show();
    $('.field-hostname').show();
    $('.field-port').show();
    $('.field-http_address').hide();
    $('.field-application_security_key').hide();
    $('.field-require_amount').hide();
}

function showHttp() {
    $('.field-tel').hide();
    $('.field-email').hide();
    $('.field-password').hide();
    $('.field-hostname').hide();
    $('.field-username').hide();
    $('.field-port').hide();
    $('.field-http_address').show();
    $('.field-application_security_key').show();
    $('.field-require_amount').show();
}

function fixDestination(destination) {
    if (destination === "1"){
        showEmail();
    }
    else if (destination === "2"){
        showTel();
    }
    else if (destination === "3"){
        showFtp();
    }
    else if (destination === "4"){
        showHttp();
    }
}


django.jQuery(document).ready(function(){
    fixDestination($('#id_destination').val())
    $('#id_destination').change(function() {
        fixDestination($(this).val());
    });





})