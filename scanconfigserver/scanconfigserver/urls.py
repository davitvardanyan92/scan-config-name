"""scanconfigserver URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from django.conf.urls import url
from confapp import views
from accounts import views as accountviews
from accounts import authentication
from rest_framework.authtoken import views as api_views
from django.conf import settings
from django.views.static import serve
from django.conf.urls.static import static


@login_required(login_url="/admin/login/")
def protected_serve(request, path, document_root=None, show_indexes=False):
    return serve(request, path, document_root, show_indexes)


# Customization on admin main page
admin.site.site_header = 'Scanner Administration'
admin.site.index_title = ''
admin.site.site_title = 'Scanner Administration'
admin.site.site_url = None


urlpatterns = [
    path('admin/', admin.site.urls),
    path('config/', views.config),
    path('params/', views.params),
    path('languages/', views.language),
    path('upload-scan/', views.HistoryView.as_view()),
    # url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    # path('history/<path:relative_path>', views.history_view),

    path('signup/', accountviews.register_user, name="register_user"),
    path('forgot-password/', accountviews.forgot_passwd, name="forgot_passwd"),
    path('reset-password/', accountviews.reset_passwd, name="reset_password"),

    path('activatecode/', accountviews.check_activation_code, name='check_activation_code'),
    path('api-token-auth/', api_views.obtain_auth_token),
    # path('api-token-auth/', accountviews.CustomAuthToken.as_view()),
    path('api/check-token/', accountviews.check_login),
    path('api-auth/', include('rest_framework.urls')),
    # url(r'^history/(?P<path>.*)$', protected_serve, {'document_root': settings.MEDIA_ROOT})
]

if settings.DEBUG:
    urlpatterns += static(settings.HISTORY_URL,
                          document_root=settings.HISTORY_ROOT)
    urlpatterns += static(settings.LANGUAGE_FILE_URL,
                          document_root=settings.LANGUAGE_FILE_ROOT)
