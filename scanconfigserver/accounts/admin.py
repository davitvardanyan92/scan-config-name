from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from accounts.models import ActivationCode, User

# Register your models here.

class CustomUserAdmin(BaseUserAdmin):
    fieldsets = BaseUserAdmin.fieldsets + (
        (None, {'fields': ('udid',)}),
    )
    add_fieldsets = BaseUserAdmin.add_fieldsets + (
        (None, {'fields': ('udid',)}),
    )
    # list_display = ("username", "udid", "email", "first_name", "last_name", "is_staff")
    list_display = ("username", "udid", "email", "is_staff")



admin.site.register(ActivationCode)
# admin.site.register(Udid)
admin.site.register(User, CustomUserAdmin)
admin.site.unregister(Group)

