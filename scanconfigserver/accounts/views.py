from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from django.http import HttpResponse, JsonResponse
from django.template.loader import render_to_string
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token

from .forms import SignUpForm, ResetPasswordForm

from .authentication import ExpiringTokenAuthentication

from .models import ActivationCode

UserModel = get_user_model()


@api_view(['GET', 'POST'])
@authentication_classes([ExpiringTokenAuthentication])
@permission_classes([IsAuthenticated])
def check_login(request, format=None):
    content = {
        'user': str(request.user),  # `django.contrib.auth.User` instance.
        'auth': str(request.auth),  # None
    }
    return Response(content)


@api_view(['POST'])
def check_activation_code(request):
    if request.method == 'POST':
        _user = request.POST['username'].lower()
        _code = request.POST['code']
        try:
            code = ActivationCode.objects.get(user__username=_user)
            if _code == code.code:
                user = UserModel._default_manager.get(username=_user)
                user.is_active = True
                user.save()
                code.delete()
                return Response({"success": "Thank you for your email confirmation. Now you can login your account."})
            # ... All set, activate & login the user, & delete the activation code
            else:
                return JsonResponse({"fail": "Activation code is incorrect."}, status=400)
        except ActivationCode.DoesNotExist:
            return JsonResponse({"fail": "code does not exist."}, status=400)


def send_activation_code(user, to_email, mail_subject, template):
    code, created = ActivationCode.objects.get_or_create(user=user)
    print(code)
    message = render_to_string(template, {
        'user': user,
        'code': code,
    })
    email = EmailMessage(
        mail_subject, message, to=[to_email]
    )
    email.send()


@api_view(['POST'])
def register_user(request):
    request.POST._mutable = True
    request.POST['username'] = request.POST['username'].lower()
    request.POST['email'] = request.POST['email'].lower()
    form = SignUpForm(request.POST)
    if form.is_valid():
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        email = form.cleaned_data.get('email')
        # Sending activation code throw email
        send_activation_code(user=user, to_email=email, mail_subject="Activate your account.", template='acc_active_email.html')

        return Response({"success": f"Please confirm your email address to complete the registration."})
    else:
        return Response({"fail": "Form is not valid."}, status=400)


@api_view(['POST'])
def forgot_passwd(request):
    _user = request.POST.get("username")
    _email = request.POST.get("email")
    if _user is not None and _email is not None:
        try:
            user = UserModel._default_manager.get(email=_email)
            send_activation_code(user=user, to_email=user.email, mail_subject="Activation code to change password.",
                                 template='forgot_passwd_email.html')
            return Response({"success": f"Please confirm your email address to complete the registration."})
        except UserModel.DoesNotExist:
            return Response({"fail": f"User {_user} does not exist."}, status=401)
    else:
        return Response({"fail": "Form is not valid."}, status=400)


@api_view(['POST'])
def reset_passwd(request):
    _user = UserModel._default_manager.get(email=request.POST.get('email'))
    _code = request.POST.get('code')
    try:
        code = ActivationCode.objects.get(user=_user)
    except:
        return Response({"fail": "Code is not valid"})
    if code.code == _code:
        form = ResetPasswordForm(data=request.POST, user=_user)
        if form.is_valid():
            user = form.save(commit=False)
            user.save()
            try:
                # If user token is expired and removed
                user.auth_token.delete()
            except Exception as e:
                pass
            code.delete()
            return Response({"success": "Password successfully changed."})
        else:
            return Response({"fail": "Reset password form is not valid"}, status=400)
    else:
        return Response({"fail": "Code is not valid."}, status=400)


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })