from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
import random
import string


def generate_activation_code():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6))


class User(AbstractUser):
    udid = models.CharField(max_length=15, blank=True)


class ActivationCode(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    code = models.CharField(max_length=6, default=generate_activation_code)

    def __str__(self):
        return self.code

    def __repr__(self):
        return self.code





# class Udid(models.Model):
#     user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
#     udid = models.CharField(max_length=15, blank=True)