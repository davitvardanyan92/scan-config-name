from django.contrib.auth.forms import UserCreationForm, SetPasswordForm
from django.contrib.auth import get_user_model

User = get_user_model()


class SignUpForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('email', 'username', 'udid')


class ResetPasswordForm(SetPasswordForm):

    class Meta:
        model = User
        fields = ('email', 'username', 'password1', 'password2', 'code')


