1) Bug - email scanned image way too big, no longer scaled to view in email client.



2) for library button custom mms, signature text is being added as a separate message, instead of being part of the original mms
	In the previous version, for mms, the signature text was displayed at the top of the image, in the current version it is being added as a separate message.

        Ideally it’s should be at the bottom of the image.

3) Login process issue 
	If you use the forgot password link, and successfully change the password, you are returned to the old login screen, which is still populated with the old failed password. 

	When you are returned to the login screen, the password field should either be empty or populated with the new password, otherwise it will confuse the user.

4) default signature text is not being read in to default mms or email images

6) can we also pass the user’s email as an additional item of meta data with the http library button type?